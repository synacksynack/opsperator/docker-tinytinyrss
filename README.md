# k8s TinyTinyRSS

TinyTinyRSS image.

Diverts from https://github.com/faust64/docker-php

Depends on postgresql or mysql, sqlite capable, can connect with
LDAP and LLNG, radosgw-ready

Build with:

```
$ make build
```

May ship with OpenShift client:

```
$ make buildoc
```

Test with:

```
$ make demo
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name               |    Description                     | Default                                                     | Inherited From    |
| :----------------------------- | ---------------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`               | TinyTinyRSS ServerName             | `rss.${OPENLDAP_DOMAIN}`                                    | opsperator/apache |
|  `APACHE_HTTP_PORT`            | TinyTinyRSS HTTP(s) Port           | `8080`                                                      | opsperator/apache |
|  `AUTH_METHOD`                 | TinyTinyRSS Main Authentication    | `local`                                                     | opsperator/apache |
|  `DO_GZIP`                     | TinyTinyRSS zlib/gzip compression  | `true`                                                      |                   |
|  `MYSQL_DATABASE`              | MySQL Database Name                | `tinytinyrss`                                               |                   |
|  `MYSQL_HOST`                  | MySQL Database Host                | `127.0.0.1`                                                 |                   |
|  `MYSQL_PASSWORD`              | MySQL Database Password            | undef                                                       |                   |
|  `MYSQL_PORT`                  | MySQL Database Port                | `3306`                                                      |                   |
|  `MYSQL_USER`                  | MySQL Database Username            | `tinytinyrss`                                               |                   |
|  `OPENLDAP_BASE`               | OpenLDAP Base                      | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`     | OpenLDAP Bind DN Prefix            | `cn=tinytinyrss,ou=services`                                | opsperator/apache |
|  `OPENLDAP_BIND_PW`            | OpenLDAP Bind Password             | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`             | OpenLDAP Domain Name               | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_HOST`               | OpenLDAP Backend Address           | undef                                                       | opsperator/apache |
|  `OPENLDAP_PORT`               | OpenLDAP Bind Port                 | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`              | OpenLDAP Proto                     | `ldap`                                                      | opsperator/apache |
|  `OPENLDAP_USERS_OBJECTCLASS`  | OpenLDAP Users ObjectClass         | `inetOrgPerson`                                             | opsperator/apache |
|  `PHP_ERRORS_LOG`              | PHP Errors Logs Output             | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`      | PHP Max Execution Time             | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`        | PHP Max File Uploads               | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`           | PHP Max Post Size                  | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE`     | PHP Max Upload File Size           | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`            | PHP Memory Limit                   | `-1` (no limitation)                                        | opsperator/php    |
|  `POSTGRES_DB`                 | Postgres Database Name             | `tinytinyrss`                                               |                   |
|  `POSTGRES_HOST`               | Postgres Database Host             | `127.0.0.1`                                                 |                   |
|  `POSTGRES_PASSWORD`           | Postgres Database Password         | undef                                                       |                   |
|  `POSTGRES_PORT`               | Postgres Database Port             | `5432`                                                      |                   |
|  `POSTGRES_USER`               | Postgres Database Username         | `tinytinyrss`                                               |                   |
|  `PUBLIC_PROTO`                | TinyTinyRSS HTTP Proto             | `http`                                                      | opsperator/apache |
|  `REG_NOTIFY`                  | TinyTinyRSS Registration notify    | `admin@$OPENLDAP_DOMAIN`                                    |                   |
|  `SIMPLE_UPDATE_MODE`          | TinyTinyRSS Simple Update Mode     | `true` - updates feeds when client queries service          |                   |
|  `SINGLE_USER_MODE`            | TinyTinyRSS Single User Moe        | `false` - disables multi-user                               |                   |
|  `SMTP_HOST`                   | TinyTinyRSS SMTP Relay             | undef                                                       |                   |
|  `SMTP_PORT`                   | TinyTinyRSS SMTP Port              | `25`                                                        |                   |
|  `SMTP_SECOPT`                 | TinyTinyRSS SMTP TLS Option        | `` (or `tls` if `SMTP_PORT=465`)                            |                   |
|  `SMTP_SKIP_VERIFY`            | TinyTinyRSS SMTP Skip TLS Verify   | `false`                                                     |                   |
|  `SPHINX_HOST`                 | TinyTinyRSS Sphinx Host            | undef                                                       |                   |
|  `SPHINX_PORT`                 | TinyTinyRSS Sphinx Port            | `9312`                                                      |                   |
|  `TINYTINYRSS_ADMIN_PASSWORD`  | TinyTinyRSS Admin Password         | `secret`                                                    |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point     | Description                         | Inherited From    |
| :---------------------- | ----------------------------------- | ----------------- |
|  `/certs`               | Apache Certificate (optional)       | opsperator/apache |
|  `/var/www/html/tt-rss` | TinyTinyRSS Runtime directory       |                   |
