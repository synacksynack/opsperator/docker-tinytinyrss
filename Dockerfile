FROM opsperator/php

# TinyTinyRSS image for OpenShift Origin

ARG DO_UPGRADE=

LABEL io.k8s.description="Tiny Tiny RSS - feeds aggregator" \
      io.k8s.display-name="TinyTinyRSS" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="tinytinyrss" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-tinytinyrss" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0"

USER root

COPY config/* /

RUN set -x \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && mkdir -p /usr/share/man/man1 /usr/share/man/man7 \
    && echo "# Install TinyTinyRSS Dependencies" \
    && apt-get -y --no-install-recommends install unzip libwebp6 s4cmd \
	postgresql-client bzip2 ldap-utils openssl libgd3 curl locales \
	locales-all libpng16-16 libjpeg62-turbo libxml2 zlib1g rsync \
	mariadb-client git \
    && if ! grep "^# ${LANG}" /etc/locale.gen >/dev/null; then \
	export LANG=en_US.UTF-8; \
    fi \
    && if ! grep "^$LANG" /etc/locale.gen >/dev/null; then \
	echo "$LANG" >>/etc/locale.gen; \
    fi \
    && touch /usr/share/locale/locale.alias \
    && locale-gen \
    && echo export LANG=${LANG} >>/etc/default/locale \
    && sed -i "s|LANG=.*|LANG=${LANG}|g" /etc/apache2/envvars \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y --no-install-recommends install libcurl4-openssl-dev \
	libfreetype6-dev libicu-dev libjpeg-dev libldap2-dev libmcrypt-dev \
	libmemcached-dev libpng-dev libpq-dev libxml2-dev libevent-dev \
	libzip-dev libwebp-dev libmagickwand-dev libssl-dev zlib1g-dev \
	libonig-dev \
    && debMultiarch="$(dpkg-architecture --query DEB_BUILD_MULTIARCH)" \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch" \
    && docker-php-ext-install curl exif intl posix opcache pcntl fileinfo dom \
        pdo_pgsql pdo_mysql mysqli pgsql zip xml mbstring session gd json ldap \
    && pecl install redis-5.3.7 \
    && docker-php-ext-enable gd redis pdo_pgsql pdo_mysql zip xml ldap \
	mbstring session dom json posix fileinfo curl mysqli pgsql \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
	| awk '/=>/ { print $3 }' | sort -u | xargs -r dpkg-query -S \
	| cut -d: -f1 | sort -u | xargs -rt apt-mark manual \
    && mv /opcache.ini /usr/local/etc/php/conf.d/opcache-recommended.ini \
    && mv /memory-limit.ini /usr/local/etc/php/conf.d/memory-limit.ini \
    && sed -i 's/MaxRequestWorkers.*/MaxRequestWorkers 30/g' \
	/etc/apache2/mods-available/mpm_prefork.conf \
    && echo "# Install TinyTinyRSS" \
    && git clone https://git.tt-rss.org/fox/tt-rss.git /usr/src/tt-rss \
    && git clone https://git.tt-rss.org/fox/ttrss-nginx-xaccel.git \
	/usr/src/tt-rss/plugins.local/nginx_xaccel \
    && git clone https://git.tt-rss.org/fox/ttrss-mailer-smtp.git \
	/usr/src/tt-rss/plugins.local/mailer_smtp \
    && git clone https://github.com/hydrian/TTRSS-Auth-LDAP.git \
	/usr/src/tt-rss/plugins.local/auth_ldap \
    && mv /usr/src/tt-rss/plugins.local/auth_ldap/plugins/auth_ldap/* \
	/usr/src/tt-rss/plugins.local/auth_ldap/ \
    && patch -d/ -p0 </auth_ldap.patch \
    && echo "# Configure TinyTinyRSS" \
    && rm -f /var/www/html/index.html \
    && mv /index.php /var/www/html/ \
    && ( \
	cd /usr/src/tt-rss \
	&& git rev-parse HEAD >git-version; \
    ) \
    && echo "# Fixing permissions" \
    && for dir in /var/www/html/tt-rss/cache /var/www/html/tt-rss/lock \
	    /var/www/html/tt-rss/feed-icons; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && chown 1001:root /usr/local/etc/php/conf.d/*.ini \
    && chmod g=u /usr/local/etc/php/conf.d/*.ini \
    && echo "# Cleaning Up" \
    && apt-get purge -y --auto-remove \
	-o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/usr/src/tt-rss/plugins.local/auth_ldap/plugins \
	/auth_ldap.patch /usr/src/php.tar.xz \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-ttrss.sh"]
USER 1001
