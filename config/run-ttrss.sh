#!/bin/sh

if test "$DEBUG"; then
    set -x
    DO_DEBUG=true
else
    DO_DEBUG=false
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
AUTH_METHOD=${AUTH_METHOD:-local}
DO_GZIP=${DO_GZIP:-true}
OIDC_CALLBACK_URL=${OIDC_CALLBACK_URL:-/oauth2/callback}
OIDC_CLIENT_ID=${OIDC_CLIENT_ID:-changeme}
OIDC_CLIENT_SECRET=${OIDC_CLIENT_SECRET:-secret}
OIDC_CRYPTO_SECRET=${OIDC_CRYPTO_SECRET:-secret}
OIDC_META_URL=${OIDC_META_URL:-/.well-known/openid-configuration}
OIDC_REMOTE_USER_CLAIM=${OIDC_REMOTE_USER_CLAIM:-sub}
OIDC_TOKEN_ENDPOINT_AUTH=${OIDC_TOKEN_ENDPOINT_AUTH:-client_secret_basic}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=tinytinyrss,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
PING_PATH=${PING_PATH:-}
PING_ROOT=${PING_ROOT:-/var/www/html}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
SIMPLE_UPDATE_MODE=${SIMPLE_UPDATE_MODE:-true}
SINGLE_USER_MODE=${SINGLE_USER_MODE:-false}
SMTP_PORT=${SMTP_PORT:-25}
SPHINX_PORT=${SPHINX_PORT:-9312}
TINYTINYRSS_ADMIN_PASSWORD="${TINYTINYRSS_ADMIN_PASSWORD:-secret}"
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=rss.$OPENLDAP_DOMAIN
fi
if test "$AUTH_METHOD" = ldap; then
    if test -z "$OPENLDAP_FILTER"; then
	OPENLDAP_FILTER="(objectClass=$OPENLDAP_USERS_OBJECTCLASS)"
    fi
fi
if ! test "$DO_GZIP" = true -o "$DO_GZIP" = false; then
    DO_GZIP=true
fi
if test -z "$REG_NOTIFY"; then
    REG_NOTIFY=admin@$OPENLDAP_DOMAIN
fi
if ! test "$SIMPLE_UPDATE_MODE" = true -o "$SIMPLE_UPDATE_MODE" = false; then
    SIMPLE_UPDATE_MODE=true
fi
if ! test "$SINGLE_USER_MODE" = true -o "$SINGLE_USER_MODE" = false; then
    SINGLE_USER_MODE=false
fi
if test "$SMTP_HOST"; then
    SMTP_ADDR=$SMTP_HOST:$SMTP_PORT
    SMTP_SKIP_VERIFY=${SMTP_SKIP_VERIFY:-false}
    if test "$SMTP_PORT" = 465; then
	SMTP_SECOPT=ssl
    elif test "$SMTP_START_TLS"; then
	SMTP_SECOPT=tls
    else
	SMTP_SECOPT=
    fi
else
    SMTP_ADDR=
    SMTP_SECOPT=
    SMTP_SKIP_VERIFY=false
fi
if test "$SPHINX_HOST"; then
    if test -z "$SPHINX_INDEX"; then
	SPHINX_INDEX="ttrss, delta"
    fi
    SPHINX_ADDR="$SPHINX_HOST:$SPHINX_PORT"
else
    SPHINX_ADDR=
    SPHINX_INDEX=
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

if test "`id -u`" = 0; then
    rsync_options="-rlDog --chown www-data:root"
else
    rsync_options=-rlD
fi
WAS_INIT=false
if ! test -s /var/www/html/tt-rss/index.php; then
    rsync $rsync_options --delete --exclude lock --exclude cache \
	--exclude feed-icons /usr/src/tt-rss/ /var/www/html/tt-rss/
    for dir in feed-icons cache lock
    do
	mkdir -p /var/www/html/tt-rss/$dir
    done
    WAS_INIT=true
else
    IMAGE_VERSION=`cat /usr/src/tt-rss/git-version 2>/dev/null`
    LOCAL_VERSION=`cat /var/www/html/tt-rss/git-version 2>/dev/null`
fi

cpt=0
LDAP_SKIP_TLS_VERIFY=false
if test "$OPENLDAP_HOST"; then
    echo Waiting for LDAP backend ...
    while true
    do
	if LDAPTLS_REQCERT=never \
		ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    echo " LDAP is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach OpenLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LDAP is ... KO
	cpt=`expr $cpt + 1`
    done
    if test "$OPENLDAP_PROTO" = ldaps; then
	if ! ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    LDAP_SKIP_TLS_VERIFY=true
	fi
    fi
    if test "$AUTH_METHOD" = lemon; then
	TTRSS_AUTH_PLUGINS=auth_remote
    elif test "$AUTH_METHOD" = ldap; then
	TTRSS_AUTH_PLUGINS="auth_ldap, auth_internal"
    else
	TTRSS_AUTH_PLUGINS=auth_internal
    fi
    cpt=0
else
    TTRSS_AUTH_PLUGINS=auth_internal
fi
if test "$POSTGRES_PASSWORD"; then
    # CREATE EXTENSION IF NOT EXISTS pg_trgm
    DB_NAME=${POSTGRES_DB:-tinytinyrss}
    DB_HOST=${POSTGRES_HOST:-127.0.0.1}
    DB_PORT=${POSTGRES_PORT:-5432}
    DB_PASS="$POSTGRES_PASSWORD"
    DB_TYPE=pgsql
    DB_USER=${POSTGRES_USER:-tinytinyrss}
    echo Waiting for Postgres backend ...
    while true
    do
	if echo '\d' | PGPASSWORD="$DB_PASS" \
		psql -U "$DB_USER" -h "$DB_HOST" \
		-p "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach Postgres
	    exit 1
	fi
	sleep 5
	echo Postgres is ... KO
	cpt=`expr $cpt + 1`
    done
    if ! echo 'SELECT * FROM ttrss_version' | PGPASSWORD="$DB_PASS" \
	    psql -U "$DB_USER" -h "$DB_HOST" \
	    -p "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	echo "Initializing database ..."
	HASHED_PW=`php /hash-password.php "$TINYTINYRSS_ADMIN_PASSWORD"`
	if ! sed "s|SHA1:5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8|$HASHED_PW|" \
		/var/www/html/tt-rss/schema/ttrss_schema_pgsql.sql \
		| PGPASSWORD="$DB_PASS" psql -U "$DB_USER" \
		-h "$DB_HOST" -p "$DB_PORT" "$DB_NAME"; then
	    echo CRITICAL: could not init database
	    exit 1
	fi
	echo NOTICE: Successfully initialized database
    else
	#FIXME / upgrades
	echo NOTICE: Database already initialized
    fi
elif test "$MYSQL_PASSWORD"; then
    DB_NAME=${MYSQL_DATABASE:-tinytinyrss}
    DB_HOST=${MYSQL_HOST:-127.0.0.1}
    DB_PASS="$MYSQL_PASSWORD"
    DB_PORT=${MYSQL_PORT:-3306}
    DB_TYPE=mysql
    DB_USER=${MYSQL_USER:-tinytinyrss}
    echo Waiting for MySQL backend ...
    while true
    do
	if echo SHOW TABLES | mysql -u "$DB_USER" \
		--password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach MySQL" >&2
	    exit 1
	fi
	sleep 5
	echo MySQL is ... KO
	cpt=`expr $cpt + 1`
    done
    if ! echo 'SELECT * FROM ttrss_version' | mysql -u "$DB_USER" \
	    --password="$DB_PASS" -h "$DB_HOST" \
	    -P "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	echo "Initializing database ..."
	HASHED_PW=`php /hash-password.php "$TINYTINYRSS_ADMIN_PASSWORD"`
	if ! sed "s|SHA1:5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8|$HASHED_PW|" \
		/var/www/html/tt-rss/schema/ttrss_schema_mysql.sql \
		| mysql -u "$DB_USER" --password="$DB_PASS" \
		-h "$DB_HOST" -P "$DB_PORT" "$DB_NAME"; then
	    echo CRITICAL: could not init database
	    exit 1
	fi
	echo NOTICE: Successfully initialized database
    else
	#FIXME / upgrades
	echo NOTICE: Database already initialized
    fi
fi

if test "$DB_TYPE"; then
    if test "$FORCE_CONF_RESET"; then
	if test -s /var/www/html/tt-rss/config.php; then
	    mv /var/www/html/tt-rss/config.php \
		/var/www/html/tt-rss/config.php.`date +%s`
	fi
    fi
    if ! test -s /var/www/html/tt-rss/config.php; then
	if ! sed -e "s|%DB_HOST%|$DB_HOST|" \
		-e "s|%DB_NAME%|$DB_NAME|" \
		-e "s|%DB_PASS%|$DB_PASS|" \
		-e "s|%DB_PORT%|$DB_PORT|" \
		-e "s|%DB_TYPE%|$DB_TYPE|" \
		-e "s|%DB_USER%|$DB_USER|" \
		-e "s|%SIMPLE_UPDATE%|$SIMPLE_UPDATE_MODE|" \
		-e "s|%SINGLE_USER%|$SINGLE_USER_MODE|" \
		-e "s|APACHE_DOMAIN|$APACHE_DOMAIN|" \
		-e "s|AUTH_PLUGINS|$TTRSS_AUTH_PLUGINS|" \
		-e "s|DO_GZIP|$DO_GZIP|" \
		-e "s|PUBLIC_PROTO|$PUBLIC_PROTO|" \
		-e "s|REG_ADDR|$REG_NOTIFY|" \
		-e "s|SPHINX_ADDR|$SPHINX_ADDR|" \
		-e "s|SPHINX_IDX|$SPHINX_INDEX|" \
		-e "s|SMTP_ADDR|$SMTP_ADDR|" \
		-e "s|SMTP_SECOPT|$SMTP_SECOPT|" \
		-e "s|SMTP_SKIP_VERIFY|$SMTP_SKIP_VERIFY|" \
		/config.sample >/var/www/html/tt-rss/config.php; then
	    echo WARNING: failed generating configuration
	else
	    echo NOTICE: Done generating configuration
	    if test "$AUTH_METHOD" = ldap -a "$OPENLDAP_HOST"; then
		if ! sed -e "s|DO_DEBUG|$DO_DEBUG|" \
			-e "s|LDAP_BASE|$OPENLDAP_BASE|" \
			-e "s|LDAP_BIND_PREFIX|$OPENLDAP_BIND_DN_PREFIX|" \
			-e "s|LDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
			-e "s#LDAP_FILTER#$OPENLDAP_FILTER#" \
			-e "s|LDAP_NO_VERIFY|$LDAP_SKIP_TLS_VERIFY|" \
			-e "s|LDAP_HOST|$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/|" \
			/ldap.sample >>/var/www/html/tt-rss/config.php; then
		    echo WARING: failed generating LDAP configuration
		else
		    echo NOTICE: Done configuring LDAP
		fi
	    fi
	fi
    else
	if test "$AUTH_METHOD" = ldap -a "$OPENLDAP_HOST"; then
	    if ! grep LDAP_AUTH /var/www/html/tt-rss/config.php >/dev/null; then
		if ! sed -e "s|DO_DEBUG|$DO_DEBUG|" \
			-e "s|LDAP_BASE|$OPENLDAP_BASE|" \
			-e "s|LDAP_BIND_PREFIX|$OPENLDAP_BIND_DN_PREFIX|" \
			-e "s|LDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
			-e "s#LDAP_FILTER#$OPENLDAP_FILTER#" \
			-e "s|LDAP_NO_VERIFY|$LDAP_SKIP_TLS_VERIFY|" \
			-e "s|LDAP_HOST|$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/|" \
			/ldap.sample >>/var/www/html/tt-rss/config.php; then
		    echo WARING: failed inserting LDAP configuration
		else
		    echo NOTICE: Done inserting LDAP
		fi
	    else
		echo NOTICE: Re-using existing configuration
	    fi
	else
	    echo NOTICE: Re-using existing configuration
	fi
    fi
else
    echo NOTICE: database type not detected - assuming site is configured
fi
if test "$OPENLDAP_HOST" = ldap; then
    export APACHE_IGNORE_OPENLDAP=yay
    if ! test -s /etc/ldap/ldap.conf; then
	if test "$OPENLDAP_PROTO" = ldaps -a "$LDAP_SKIP_TLS_VERIFY"; then
	    cat <<EOF
URI		$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
TLS_REQCERT	never
EOF
	elif test "$OPENLDAP_PROTO" = ldaps; then
	    cat <<EOF
URI		$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
TLS_REQCERT	demand
EOF
	fi >/etc/ldap/ldap.conf
    fi
fi
if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    if test "$AUTH_METHOD" = lemon; then
	VHST=/lemon-vhost.conf
    elif test "$AUTH_METHOD" = oidc; then
	VHST=/oidc-vhost.conf
	if ! echo "$OIDC_CALLBACK_URL" | grep ^/ >/dev/null; then
	    OIDC_CALLBACK_URL=/$OIDC_CALLBACK_URL
	fi
	CALLBACK_ROOT_SUB_COUNT=`echo $OIDC_CALLBACK_URL | awk -F/ '{print NF}'`
	CALLBACK_ROOT_SUB_COUNT=`expr $CALLBACK_ROOT_SUB_COUNT - 2 2>/dev/null`
	if ! test "$CALLBACK_ROOT_SUB_COUNT" -ge 2; then
	    CALLBACK_ROOT_SUB_COUNT=2
	fi
	CALLBACK_ROOT_URL=`echo $OIDC_CALLBACK_URL | cut -d/ -f 2-$CALLBACK_ROOT_SUB_COUNT`
    else
	VHST=/vhost.conf
    fi
    echo Generates Tiny Tiny RSS VirtualHost Configuration
    sed -e "s|HTTP_PORT|$APACHE_HTTP_PORT|" \
	-e "s|DOMAIN|$APACHE_DOMAIN|" \
	-e "s|CALLBACK_ROOT_URL|$CALLBACK_ROOT_URL|" \
	-e "s|OIDC_CALLBACK_URL|$OIDC_CALLBACK_URL|" \
	-e "s|OIDC_CLIENT_ID|$OIDC_CLIENT_ID|" \
	-e "s|OIDC_CLIENT_SECRET|$OIDC_CLIENT_SECRET|" \
	-e "s|OIDC_CRYPTO_SECRET|$OIDC_CRYPTO_SECRET|" \
	-e "s|OIDC_META_URL|$OIDC_META_URL|" \
	-e "s|OIDC_PORTAL|$OIDC_PORTAL|" \
	-e "s|OIDC_REMOTE_USER_CLAIM|$OIDC_REMOTE_USER_CLAIM|" \
	-e "s|OIDC_TOKEN_ENDPOINT_AUTH|$OIDC_TOKEN_ENDPOINT_AUTH|" \
	-e "s|PING_PATH|$PING_PATH|" \
	-e "s|PING_ROOT|$PING_ROOT|" \
	-e "s|PUBLIC_PROTO|$PUBLIC_PROTO|" \
	-e "s|SSL_INCLUDE|$SSL_INCLUDE|" \
	$VHST >/etc/apache2/sites-enabled/003-vhosts.conf
fi
for d in images upload export
do
    if ! test -d "/var/www/html/tt-rss/cache/$d"; then
	mkdir -p "/var/www/html/tt-rss/cache/$d"
	chmod g=u "/var/www/html/tt-rss/cache/$d"
    fi
done
mkdir -p /var/www/html/ping
echo OK >/var/www/html/index.html
echo OK >/var/www/html/ping/index.html

if ! $WAS_INIT; then
    if test "$IMAGE_VERSION" != "$LOCAL_VERSION"; then
	echo NOTICE: runnning site upgrade
	rsync $rsync_options --delete --exclude lock --exclude cache \
	    --exclude feed-icons --exlucde config.php \
	    /usr/src/tt-rss/ /var/www/html/tt-rss/
	(
	    cd /var/www/html/tt-rss/
	    echo NOTICE: Now upgrading database, this may take a while ...
	    echo "started on $(date)" >/var/www/html/tt-rss/.db-upgrade.lock
	    /usr/local/bin/php ./update.php --update-schema=force-yes
	    rm -f /var/www/html/tt-rss/.db-upgrade.lock
	    echo NOTICE: Done running database upgrade
	) &
    fi
fi

unset MYSQL_USER MYSQL_PASSWORD POSTGRES_USER POSTGRES_PASSWORD DO_GZIP cpt \
    SINGLE_USER_MODE SIMPLE_UPDATE_MODE AUTH_METHOD DB_NAME DB_HOST DB_PASS \
    DB_PORT DB_TYPE DB_USER WAS_INIT IMAGE_VERSION LOCAL_VERSION HASHED_PW \
    LDAP_SKIP_TLS_VERIFY TTRSS_AUTH_PLUGINS OPENLDAP_FILTER \
    TINYTINYRSS_ADMIN_PASSWORD

. /run-apache.sh
