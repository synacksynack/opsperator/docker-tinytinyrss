#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
. /usr/local/bin/nsswrapper.sh

if test -s /usr/local/bin/phpinit.sh; then
    . /usr/local/bin/phpinit.sh
fi

SIMPLE_UPDATE_MODE=${SIMPLE_UPDATE_MODE:-true}
if test "$POSTGRES_PASSWORD"; then
    DB_NAME=${POSTGRES_DB:-tinytinyrss}
    DB_HOST=${POSTGRES_HOST:-127.0.0.1}
    DB_PORT=${POSTGRES_PORT:-5432}
    DB_PASS="$POSTGRES_PASSWORD"
    DB_TYPE=pgsql
    DB_USER=${POSTGRES_USER:-tinytinyrss}
    echo Waiting for Postgres backend ...
    while true
    do
	if echo '\d' | PGPASSWORD="$DB_PASS" \
		psql -U "$DB_USER" -h "$DB_HOST" \
		-p "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach Postgres
	    exit 1
	fi
	sleep 5
	echo Postgres is ... KO
	cpt=`expr $cpt + 1`
    done
elif test "$MYSQL_PASSWORD"; then
    DB_NAME=${MYSQL_DATABASE:-tinytinyrss}
    DB_HOST=${MYSQL_HOST:-127.0.0.1}
    DB_PASS="$MYSQL_PASSWORD"
    DB_PORT=${MYSQL_PORT:-3306}
    DB_TYPE=mysql
    DB_USER=${MYSQL_USER:-tinytinyrss}
    echo Waiting for MySQL backend ...
    while true
    do
	if echo SHOW TABLES | mysql -u "$DB_USER" \
		--password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach MySQL" >&2
	    exit 1
	fi
	sleep 5
	echo MySQL is ... KO
	cpt=`expr $cpt + 1`
    done
fi

if test "$SIMPLE_UPDATE_MODE" = false; then
    if ! test "$UPDATE_INTERVAL" -gt 59 >/dev/null 2>&1; then
	UPDATE_INTERVAL=300
    fi
fi

unset MYSQL_USER MYSQL_PASSWORD POSTGRES_USER POSTGRES_PASSWORD DB_PORT \
    SINGLE_USER_MODE SIMPLE_UPDATE_MODE AUTH_METHOD DB_NAME DB_HOST DB_PASS \
    DB_TYPE DB_USER

while ! test -s /var/www/html/tt-rss/config.php
do
    echo NOTICE: Waiting for configuration
    sleep 10
done

cd /var/www/html/tt-rss
if test "$UPDATE_INTERVAL" -a "$UPDATE_INTERVAL" -gt 59 >/dev/null 2>&1; then
    while sleep "$UPDATE_INTERVAL"
    do
	while test -s /var/www/html/tt-rss/.db-upgrade.lock
	do
	    echo NOTICE: Waiting for database upgrade lock to go away
	    sleep 30
	done
	echo "NOTICE: Running tt-rss feeds update job (`date`)"
	/usr/local/bin/php /var/www/html/tt-rss/update_daemon2.php
    done
fi

/usr/local/bin/php /var/www/html/tt-rss/update_daemon2.php

exit $?
